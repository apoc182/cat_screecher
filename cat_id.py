import requests
from json import loads

def cat_in_image(image):

    endpoint = "http://api.thecatapi.com/v1/images/upload"
    delete_endpoint = "https://api.thecatapi.com/v1/images/"

    files = {"file" : ("test_cat.jpg", open(image, "rb"), "image/jpeg")}

    headers = {
        'x-api-key': "dd6ce80e-b991-4bc8-9e70-feb137acdaba"
    }

    r = requests.post(url = endpoint, files=files, headers=headers) 

    # Test if a cat in image.
    if "id" in r.text:
        json_data = loads(r.text)
        r = requests.delete(delete_endpoint + json_data["id"], headers=headers)
        if r.status_code != 204:
            print("There was an error removing the image.")
        return True
    else:
        return False


if __name__ == "__main__":
    print("Cat in image: " + str(cat_in_image("small.jpg")))
    print("Cat in image: " + str(cat_in_image("test_cat.jpg")))
    print("Cat in image: " + str(cat_in_image("image.jpg")))