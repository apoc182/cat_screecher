import imutils
import cv2
import numpy as np
import pygame
from datetime import datetime
from time import sleep
from email_user import notify_of_cat
import argparse
import os
from random import choice

# =============================================================================
# USER-SET PARAMETERS
# =============================================================================

# Number of frames to pass before changing the frame to compare the current
# frame against
FRAMES_TO_PERSIST = 10

# Minimum boxed area for a detected motion to count as actual motion
# Use to filter out noise or small objects
MIN_SIZE_FOR_MOVEMENT = 2000

# Minimum length of time where no motion is detected it should take
#(in program cycles) for the program to declare that there is no movement
MOVEMENT_DETECTED_PERSISTENCE = 100


# Simple logging.
def log_it(event):
    now = datetime.now()
    date = now.strftime('%Y%m%d')
    timestamp = now.strftime("%d/%m/%Y %H:%M:%S")
    output =  timestamp + " || " + event + "\n"
    with open("Log" + date + ".txt", "a+") as f:
        f.write(output)
    return timestamp

log_it("Application started.")

def main(email=False):
    # Prepare pygame sound mixer.
    pygame.mixer.init()

    sound_files = []
    # Get all the mp3's
    for item in os.listdir("."):
        if ".mp3" in item:
            sound_files.append(item)
    

    # =============================================================================
    # CORE PROGRAM
    # =============================================================================


    # Create capture object
    cap = cv2.VideoCapture(5) # Flush the stream
    cap.release()
    cap = cv2.VideoCapture(0) # Then start the webcam

    # Init frame variables
    first_frame = None
    next_frame = None

    # Init display font and timeout counters
    font = cv2.FONT_HERSHEY_SIMPLEX
    delay_counter = 0
    movement_persistent_counter = 0

    # LOOP!
    while True:

        # Set transient motion detected as false
        transient_movement_flag = False
        
        # Read frame
        ret, frame = cap.read()
        text = "Unoccupied"

        # If there's an error in capturing
        if not ret:
            print("CAPTURE ERROR")
            continue

        # Resize and save a greyscale version of the image
        frame = imutils.resize(frame, width = 750)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Blur it to remove camera noise (reducing false positives)
        gray = cv2.GaussianBlur(gray, (21, 21), 0)

        # If the first frame is nothing, initialise it
        if first_frame is None: first_frame = gray    

        delay_counter += 1

        # Otherwise, set the first frame to compare as the previous frame
        # But only if the counter reaches the appriopriate value
        # The delay is to allow relatively slow motions to be counted as large
        # motions if they're spread out far enough
        if delay_counter > FRAMES_TO_PERSIST:
            delay_counter = 0
            first_frame = next_frame

            
        # Set the next frame to compare (the current frame)
        next_frame = gray

        # Compare the two frames, find the difference
        frame_delta = cv2.absdiff(first_frame, next_frame)
        thresh = cv2.threshold(frame_delta, 25, 255, cv2.THRESH_BINARY)[1]

        # Fill in holes via dilate(), and find contours of the thesholds
        thresh = cv2.dilate(thresh, None, iterations = 2)
        cnts, _ = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        # loop over the contours
        for c in cnts:

            # Save the coordinates of all found contours
            (x, y, w, h) = cv2.boundingRect(c)
            
            # If the contour is too small, ignore it, otherwise, there's transient
            # movement
            if cv2.contourArea(c) > MIN_SIZE_FOR_MOVEMENT:
                transient_movement_flag = True
                
                # Draw a rectangle around big enough movements
                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

        # The moment something moves momentarily, reset the persistent
        # movement timer.
        if transient_movement_flag == True:
            movement_persistent_flag = True
            movement_persistent_counter = MOVEMENT_DETECTED_PERSISTENCE

        # As long as there was a recent transient movement, say a movement
        # was detected    
        if movement_persistent_counter > 0:
            if not pygame.mixer.music.get_busy() and movement_persistent_counter == 100:
                pygame.mixer.music.load(choice(sound_files))
                pygame.mixer.music.play()
                ts = log_it("Cat detected!")
                if email:
                    cv2.imwrite("image.jpg", frame)
                    notify_of_cat("image.jpg")
            movement_persistent_counter -= 1
        else:
            text = "No Movement Detected"

    log_it("Application terminated.")

    # Cleanup when closed
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    cap.release()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Checks for them pesky cats on a bench and gives them a fright!')
    parser.add_argument('-e', '--email', help='If you want to be notified of their behaviour via email.', action="store_true")
    args = parser.parse_args()

    if args.email:
        main(email=True)
    else:
        main()
